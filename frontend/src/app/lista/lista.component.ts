import { ListaService } from './lista.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.scss']
})
export class ListaComponent implements OnInit {

  company: any = [];
  offset: number = 0;

  constructor(private listaservice: ListaService) {
  }

  ngOnInit(): void {
  }

  getCompanies() {
    this.getAll();
  }


  getAll() {
    this.listaservice.getCompany().subscribe(
      (success) => {
        this.company = success;
      },
      (error) => { console.log(error) }
    );
  }

}
