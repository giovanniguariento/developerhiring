import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ListaService {

  url = environment.api + 'standardDeviation'

  constructor(private http: HttpClient) {

  }


  getCompany() {
    return this.http.get(this.url);

  }
  

}