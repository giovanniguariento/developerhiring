package backend

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class CompanyController {

    CompanyService companyService
    StocksByCompanyService stocksByCompanyService

    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond companyService.list(params), model:[companyCount: companyService.count()]
    }

    def show(Long id) {
        respond companyService.get(id)
    }

    def getStocksByCompany(String companyName, int numberOfHours){
       respond stocksByCompanyService.getStocks(companyService.get(companyName), numberOfHours)
    }
    
//    def getStandardDeviation(String companyName){
//       respond stocksByCompanyService.getStandardDeviation(companyService.get(companyName))
//    }
    
    def getStandardDeviation(){
        respond stocksByCompanyService.getStandardDeviation()
    }

}
