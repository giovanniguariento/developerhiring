package backend

class Company {
    String name
    String segment

    static constraints = {
        name nullable: false
        segment nullable: false
    }
    
    Company(){
        
    }
    
    Company(String _name, String _segment){
        name = _name
        segment = _segment
    }
}
