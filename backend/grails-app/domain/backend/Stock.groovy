package backend

import java.time.LocalDateTime

class Stock {
    BigDecimal price
    LocalDateTime priceDate
    
    static belongsTo = [ company: Company ]

    static constraints = {
        price nullable: false
        priceDate nullable: false
    }
    
    Stock(){
        
    }
    
    Stock(BigDecimal _price, LocalDateTime _priceDate, Company _company){
        price = _price
        priceDate = _priceDate
        company = _company
    }
}
