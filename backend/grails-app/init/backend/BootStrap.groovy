package backend

import java.time.LocalDateTime

class BootStrap {

    StockService stockService
    CompanyService companyService
    
    def init = { servletContext ->
        LocalDateTime day = LocalDateTime.now().withHour(10).withMinute(0).withSecond(0);
        Random random = new Random()
        int maxPrice = 100;
        int days = 30;
        int dayMinutes = 480;
        
        def Company ford = new Company('Ford', 'vehicles').save()
        def Company nestle = new Company('Nestle', 'food').save()
        def Company nissan = new Company('Nissan', 'vehicles').save()
        
        for(int j=0; j<days; j++){
            for(int i=0; i<dayMinutes; i++){
                stockService.save((BigDecimal)random.nextFloat() * random.nextInt(maxPrice), day, ford).save()
                stockService.save((BigDecimal)random.nextFloat() * random.nextInt(maxPrice), day, nestle).save()
                stockService.save((BigDecimal)random.nextFloat() * random.nextInt(maxPrice), day, nissan).save()

                day = day.plusMinutes(1);
            }
            day = day.minusDays(1).withHour(10).withMinute(0);
        }
    }
    def destroy = {
    }
}
