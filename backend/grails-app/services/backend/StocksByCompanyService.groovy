package backend

import grails.gorm.transactions.Transactional
import groovy.json.*

@Transactional
class StocksByCompanyService {

    def getStocks(Company company, int numbersOfHoursUntilNow){
        long now = System.currentTimeMillis();
        int totalMinutesUntilNow = 60*numbersOfHoursUntilNow
        def results = Stock.findAllByCompany(company,
                 [max: totalMinutesUntilNow, sort: "priceDate", order: "desc"])
             for(result in results){
                 log.info 'Stock quote id: ' + result.id.toString() + ' price: ' + result.price.toString() +
                    ' priceDate: s' + result.priceDate.toString() + ' company: ' + result.company.toString()
             }
             log.info 'Total time to execute service: ' + (System.currentTimeMillis() - now) + 'ms'
             log.info 'Number of quotes retrieved: ' + results.size().toString()
             return results;
    }
    
    def getStandardDeviation(){
        def companyList = Company.getAll()
        def response = '[';
        
        log.info 'Number of companys retrieved: ' + companyList.size().toString()
        
        for(company in companyList){
            def results = Stock.findAllByCompany(company,
                     [sort: "priceDate", order: "desc"])

            log.info 'Number of quotes retrieved: ' + results.size().toString()

            double sum = 0
            double newSum = 0

            for (result in results) {
                sum = sum + result.price
            }
            double mean = (sum) / results.size()

            for (result in results) {
                // put the calculation right in there
                newSum = newSum + ((result.price - mean) * (result.price - mean))
            }
            double squaredDiffMean = (newSum) / results.size()
            double standardDev = (Math.sqrt(squaredDiffMean))

            
            def str = '{"name":  "' + company.name + '","segment": "' + company.segment + '","deviation": ' + standardDev + '},'
            
            response = response + str;
        }
        response = response.substring(0,response.length() - 1);
        response = response + ']'
        def parser = new JsonSlurper()
        def json = parser.parseText(response)  
        return json
    }
}
