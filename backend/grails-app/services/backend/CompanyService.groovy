package backend

import grails.gorm.services.Service

@Service(Company)
interface CompanyService {

    Company get(Serializable id)
    
    Company get(String name)

    List<Company> list(Map args)

    Long count()
  
}